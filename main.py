from bs4 import BeautifulSoup
from selenium.webdriver import Chrome
import os
import re
import time

# init of variables used throughout scraper
programStartTime = time.time()
path, filename = os.path.split(os.path.realpath(__file__))
exePath = path.replace('\\', '/') + '/chromedriver/chromedriver.exe'
_RE_COMBINE_WHITESPACE = re.compile(r"\s+")
driver = Chrome(executable_path=exePath)
url = 'https://www.ultimate-guitar.com/explore?order=hitstotal_desc&type[]=Chords'
processedSongs = []
scrapedPages = 20

# loading page via selenium
driver.get(url)
driver.maximize_window()

# loop for scraping first 3 pages of table with songs
for i in range(scrapedPages):
    print('Scraping page {0} of {1}'.format(i, scrapedPages))
    pageStartTime = time.time()

    # init doc for page containing table with songs
    doc = BeautifulSoup(driver.page_source, 'lxml')

    # finding all rows of table with songs, if found loop through them
    songs = doc.find_all('div', class_='_3uKbA')
    if(songs):
        for song in songs:
            # data structure which will be loaded in next steps
            data = {
                'Artist': '',
                'Title': '',
                'Difficulty': '',
                'Tuning': '',
                'Capo': '',
                'Key': '',
                'Chords': '',
                'ChordsSequence': '',
                'Lyrics': ''
            }

            # finding elements containing artist name and song title, if found processed them
            artist = song.find('a', class_='_3DU-x hn34w _3dYeW')
            title = song.find('a', class_='_3DU-x JoRLr _3dYeW')
            if(artist and title):
                print('Scraping song {0} by {1}'.format(title.text.encode('utf-8').strip(), artist.text.encode('utf-8').strip()))
                songStartTime = time.time()
                # scrap artist name
                data['Artist'] = artist.text.encode('utf-8').strip()

                # scrap song title, if title contains version enclosed in () remove it
                if(title.text.encode('utf-8').strip().find('(') != -1):
                    data['Title'] = title.text.encode('utf-8').strip().replace(title.text.encode('utf-8').strip()[title.text.encode('utf-8').strip().find('('):title.text.encode('utf-8').strip().find(')')+1], '')
                else:
                    data['Title'] = title.text.encode('utf-8').strip()
                
                # save current window handle, open new tab an load song detail by clicking on the song title
                original_window = driver.current_window_handle
                driver.execute_script("window.open('');")
                driver.switch_to.window(driver.window_handles[1])
                driver.get(title['href'].strip())

                # init doc for page with details of the song
                detailDoc = BeautifulSoup(driver.page_source, 'lxml')

                # finding element containing songs setup, if found add to the data structure
                setup = detailDoc.find('div', class_='tDXKF')
                if(setup):
                    for section in setup:
                        sectionName = section.text.split(' ', 1)[0].encode('utf-8').strip().replace(':', '')
                        data[sectionName] =  section.text.split(' ', 1)[1].encode('utf-8').strip()
                
                # finding element containing songs chords, if found add to the data structure
                chords = detailDoc.find_all('header', class_='_1s0F3')
                if(chords):
                    for chord in chords:
                        data['Chords'] += chord.text.encode('utf-8').strip() + ' '
                data['Chords'] = _RE_COMBINE_WHITESPACE.sub(" ", data['Chords']).strip()

                # finding element containing songs lyrics, if found add to the data structure
                elementsWithLyricInside = detailDoc.find_all('span', class_='_2jIGi')
                if(elementsWithLyricInside):
                    for element in elementsWithLyricInside:
                        if('|' not in element.findChildren()[-1].text.encode('utf-8').strip()):
                            data['Lyrics'] += element.findChildren()[-1].text.encode('utf-8').strip() + ' '
                data['Lyrics'] = _RE_COMBINE_WHITESPACE.sub(" ", data['Lyrics']).strip()
                data['Lyrics'] = data['Lyrics'].replace(';', ',')

                # finding element containing sequence of chords, if found add to the data structure
                playedChords = detailDoc.find_all('span', class_='_3PpPJ OrSDI')
                if(playedChords):
                    for playedChord in playedChords:
                        data['ChordsSequence'] += playedChord.text.encode('utf-8').strip() + ' '
                data['ChordsSequence'] = _RE_COMBINE_WHITESPACE.sub(" ", data['ChordsSequence']).strip()

                print('Song {0} by {1} scraped in {2}s'.format(title.text.encode('utf-8').strip(), artist.text.encode('utf-8').strip(), time.time() - songStartTime))
                # close tab with song detail, switch to main page and save scraped data
                driver.close()
                driver.switch_to.window(original_window)
                processedSongs.append(data)

    print("Page {0} of {1} scraped in {2}s".format(i, scrapedPages, time.time() - pageStartTime))

    # finding button to load next songs in table, if found click on it  
    nextButton = doc.find('a', class_='-rNj1 _2cDWo _2-eSi _3DU-x hn34w _3dYeW')
    if(nextButton):
        driver.get(nextButton['href'])

driver.quit()

# save scraped data to csv file
with open(path + '/scraped_songs.csv', 'w') as file:
    file.write('Artist; Title; Difficulty; Tuning; Capo; Key; Chords; ChordsSequence; Lyrics \n')
    for processedSong in processedSongs:
        file.write(processedSong['Artist'] + ';' + processedSong['Title'] + ';' + processedSong['Difficulty'] + ';' + processedSong['Tuning'] + ';' + processedSong['Capo'] + ';' + processedSong['Key'] + ';' + processedSong['Chords'] + ';' + processedSong['ChordsSequence'] + ';' + processedSong['Lyrics'] + '\n')

print("Scraping done. Total run time of {0}s".format(time.time() - programStartTime))